import { useMemo } from 'react';
import { useRouter } from 'next/router';
import { IntlProvider, FormattedMessage } from 'react-intl';
import Spanish from '../content/compiled-locales/es.json';
import English from '../content/compiled-locales/en.json';

import '../styles/globals.css';

function MyApp({ Component, pageProps }) {
  const { locale } = useRouter();
  const [shortLocale] = locale ? locale.split('-') : ['en'];

  const messages = useMemo(() => {
    switch (shortLocale) {
      case 'es':
        return Spanish;
      case 'en':
        return English;
      default:
        return English;
    }
  }, [shortLocale]);

  return (
    <IntlProvider locale={shortLocale} messages={messages} onError={() => null}>
      <FormattedMessage defaultMessage='Good morning' id='good.morning' />
      <Component {...pageProps} />
    </IntlProvider>
  );
}

export default MyApp;
